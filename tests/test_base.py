from context import package as p
import pytest,itertools

class List(list):
    pass

def test_list_init(l):
    assert l == p.ReadOnlyListProxy(l)
    assert l == p.ReadOnlyListProxy(List(l))
    with pytest.raises(TypeError):
        _ = p.ReadOnlyListProxy(iter(l))

def test_list(l):
    L = p.ReadOnlyListProxy(l)
    assert l == L
    for x,y in zip(l,L):
        assert x == y
    x = l.pop()
    l[l[0]%len(l)] += x
    for x,y in zip(l,L):
        assert x == y
    m,n = l+[x],L+[x]
    assert m == n
    assert m is not n
    assert type(n) == list
    for x in l[:10]:
        assert x in L
    assert l[l[0]%len(l):] == L[L[0]%len(L):]
    assert format(L) == str(l)
    assert '%s(%s)' % (type(L).__name__,repr(l)) == str(L)
    assert '%s(%s)' % (type(L).__name__,repr(l)) == repr(L)
    k,m = list(l),list(l)
    k[k[0]%len(k)]-=k[1]%2
    m[m[0]%len(k)]+=m[2]%2
    assert k <= l <= m
    assert k <= L <= m
    k[k[0]%len(k)]-=1
    m[m[0]%len(k)]+=1
    assert k != L
    assert k < l < m
    assert k < L < m
    assert l*2 == L*2
    assert 2*l == 2*L
    assert list(reversed(l)) == list(reversed(L))
    m = L.copy()
    assert l == m
    m.pop()
    assert l != m
    assert l.count(l[0]) == L.count(L[0])
    assert l.index(l[-1]) == L.index(L[-1])

class Dict(dict):
    pass

def test_dict_init(d):
    assert d == p.ReadOnlyDictProxy(d)
    assert d == p.ReadOnlyDictProxy(Dict(d))
    with pytest.raises(TypeError):
        _ = p.ReadOnlyDictProxy(d.items())

def test_dict(d):
    D = p.ReadOnlyDictProxy(d)
    assert (d == D) and not (d != D)
    for x,y in zip(d.items(),D.items()):
        assert x == y
    k,v = d.popitem()
    d[list(d.keys())[v%len(D)]] += v
    for k,v,l,w in zip(d.keys(),d.values(),D.keys(),D.values()):
        assert (k,v) == (l,w)
    for k,v in itertools.islice(D.items(),10):
        assert k in d
        assert k in D
        assert d[k] == v
        assert D[k] == v
    assert format(D) == str(d)
    assert '%s(%s)' % (type(D).__name__,repr(d)) == str(D)
    assert '%s(%s)' % (type(D).__name__,repr(d)) == repr(D)
    e = D.copy()
    assert d == e
    assert d is not e
    assert type(e) == dict
    for k,l in itertools.islice(zip(D,D.keys()),10):
        assert k == l
        assert D.get(k) == e[l]
