import pytest,random

def getranddict(r,**kw):
    min_depth = kw.setdefault('min_depth',0)
    kw['min_depth']-=1
    max_depth = max(min_depth,int(kw.get('max_depth',1)))
    kw['max_depth']=max_depth-1
    v_bits = max(4,int(kw.get('v_bits',0)))
    kw['v_bits'] = v_bits
    min_width = kw.setdefault('min_width',0)
    max_width = max(min_width,int(kw.get('max_width',10)))
    kw['max_width'] = max_width
    k_bits = max(4,int(kw.get('k_bits',0)))
    kw['k_bits'] = k_bits
    if min_depth+r.randrange(1+max_depth-min_depth) <= 0:
        return r.getrandbits(v_bits)
    d = {}
    for _ in range(r.randrange(min_width,1+max_width)):
        k = hex(r.getrandbits(k_bits))[2:]
        v = r.getranddict(**kw)
        d[k] = v
    return d

random.Random.getranddict = getranddict

def random_list(seed,size,nbits=20):
    r = random.Random(seed)
    l = list(r.getrandbits(nbits) for _ in range(size))
    return l

@pytest.fixture(params=random_list(14759,100))
def seed(request):
    return request.param

@pytest.fixture(params=zip(random_list(26491,100),random_list(15537,100,nbits=12)))
def l(request):
    seed,size = request.param
    size = max(10,size)
    return random_list(seed,size)

@pytest.fixture(params=random_list(52938,100))
def d(request):
    return random.Random(request.param)\
            .getranddict(min_depth=1,max_depth=1,
                    k_bits=32,v_bits=20,
                    min_width=5,max_width=100)
