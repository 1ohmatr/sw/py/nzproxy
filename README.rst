|pipeline| |coverage| |rtd|

.. |pipeline| image:: https://framagit.org/1ohmatr/sw/py/nzproxy/badges/master/pipeline.svg

.. |coverage| image:: https://framagit.org/1ohmatr/sw/py/nzproxy/badges/master/coverage.svg

.. |rtd| image:: https://readthedocs.org/projects/nzproxy/badge/?version=latest

Proxies for python objects.
